
window.keylay={
  keys:{
  }
  ,cfg:{
    passvals:['r','x','m','w','h','t','l']
  }
  ,classmap:{
    u:'unshift'
  }
  ,order:[]
  ,kbds:{
    ducky:{
      vals:{
        m:{
          'ESC':1
          ,'F4':0.5
          ,'F8':0.5
          ,'F12':0.25
          ,'PAUSE':0.25
          ,'BACKSPACE':0.25
          ,'PGUP':0.25
          ,'BSL':0.25
          ,'PGDN':0.25
          ,'ENTER':3.5
          ,'RSHIFT':1.25
          ,'UP':1.25
          ,'RCTRL':0.25
          ,'RIGHT':0.25
        }
        ,w:{
          'BACKSPACE':2
          ,'TAB':1.5
          ,'BSL':1.5
          ,'VBAR':1.5
          ,'CAPS':1.75
          ,'ENTER':2.25
          ,'LSHIFT':2.25
          ,'RSHIFT':2.75
          ,'LCTRL':1.25
          ,'LWIN':1.25
          ,'LALT':1.25
          ,'SPACE':6.25
          ,'RALT':1.25
          ,'RWIN':1.25
          ,'FN':1.25
          ,'RCTRL':1.25
          ,'NUM0':2
        }
        ,h:{
          'ADD':2
          ,'NUMENTER':2
        }
        ,l:{
          'q':1.5
          ,'a':1.75
          ,'z':2.25
        }
        ,t:{
          'BQ':'`'
          ,'DASH':'-'
          ,'EQUALS':'='
          ,'DIVIDE':'/'
          ,'MULTIPLY':'*'
          ,'SUBTRACT':'-'
          ,'TILDE':'~'
          ,'BANG':'!'
          ,'AT':'@'
          ,'HASH':'#'
          ,'BUCK':'$'
          ,'PCT':'%'
          ,'CARET':'^'
          ,'AMP':'&'
          ,'AST':'*'
          ,'LP':'('
          ,'RP':')'
          ,'BAR':'_'
          ,'PLUS':'+'
          ,'LBKT':'['
          ,'RBKT':']'
          ,'BSL':'\\'
          ,'NUM7':'7'
          ,'NUM8':'8'
          ,'NUM9':'9'
          ,'LBRC':'{'
          ,'RBRC':'}'
          ,'VBAR':'|'
          ,'SEMI':';'
          ,'SQ':"'"
          ,'NUM4':'4'
          ,'NUM5':'5'
          ,'NUM6':'6'
          ,'COLON':':'
          ,'QUOTE':'"'
          ,'LSHIFT':'SHIFT'
          ,'RSHIFT':'SHIFT'
          ,'COMMA':','
          ,'PERIOD':'.'
          ,'FSL':'/'
          ,'NUM1':'1'
          ,'NUM2':'2'
          ,'NUM3':'3'
          ,'LT':'<'
          ,'GT':'>'
          ,'QM':'?'
          ,'LCTRL':'CTRL'
          ,'LWIN':'WIN'
          ,'LALT':'ALT'
          ,'RALT':'ALT'
          ,'RWIN':'WIN'
          ,'RCTRL':'CTRL'
          ,'NUM0':'0'
          ,'DECIMAL':'.'
        }
        ,shifter:{
          'TILDE':1,'BANG':1,'AT':1,'HASH':1,'BUCK':1,'PCT':1,'CARET':1,'AMP':1,'AST':1,'LP':1,'RP':1,'BAR':1,'PLUS':1,'LBRC':1,'RBRC':1,'VBAR':1,'COLON':1,'QUOTE':1,'LT':1,'GT':1,'QM':1
        }
        ,lowcase:{
          'a':1,'b':1,'c':1,'d':1,'e':1,'f':1,'g':1,'h':1,'i':1,'j':1,'k':1,'l':1,'m':1,'n':1,'o':1,'p':1,'q':1,'r':1,'s':1,'t':1,'u':1,'v':1,'w':1,'x':1,'y':1,'z':1
        }
        ,upcase:{
          'A':1,'B':1,'C':1,'D':1,'E':1,'F':1,'G':1,'H':1,'I':1,'J':1,'K':1,'L':1,'M':1,'N':1,'O':1,'P':1,'Q':1,'R':1,'S':1,'T':1,'U':1,'V':1,'W':1,'X':1,'Y':1,'Z':1
        }
        ,numnum:{
          '1':1,'2':1,'3':1,'4':1,'5':1,'6':1,'7':1,'8':1,'9':1,'0':1
        }
        ,nums:{
          '1':1,'2':1,'3':1,'4':1,'5':1,'6':1,'7':1,'8':1,'9':1,'0':1,'NUM1':1,'NUM2':1,'NUM3':1,'NUM4':1,'NUM5':1,'NUM6':1,'NUM7':1,'NUM8':1,'NUM9':1,'NUM0':1
        }
        ,numpadnum:{
          'NUM1':1,'NUM2':1,'NUM3':1,'NUM4':1,'NUM5':1,'NUM6':1,'NUM7':1,'NUM8':1,'NUM9':1,'NUM0':1
        }
        ,numpad:{
          'NUMLK':1,'DIVIDE':1,'MULTIPLY':1,'SUBTRACT':1,'ADD':1,'NUMENTER':1,'NUM1':1,'NUM2':1,'NUM3':1,'NUM4':1,'NUM5':1,'NUM6':1,'NUM7':1,'NUM8':1,'NUM9':1,'NUM0':1
        }
        ,functionkey:{
          'F1':1,'F2':1,'F3':1,'F4':1,'F5':1,'F6':1,'F7':1,'F8':1,'F9':1,'F10':1,'F11':1,'F12':1
        }
        ,mods:{
          'LSHIFT':1,'RSHIFT':1,'LCTRL':1,'RCTRL':1,'LALT':1,'RALT':1,'LWIN':1,'RWIN':1,'FN':1
        }
        ,lock:{
          'CAPS':1,'NUMLK':1,'SCRLK':1
        }
        ,meta:{
          'X1':1,'X2':1,'X3':1,'X4':1
        }
        ,action:{
          'ESC':1,'ENTER':1,'INS':1,'DEL':1,'BACKSPACE':1
        }
        ,az:{
          'a':1,'b':1,'c':1,'d':1,'e':1,'f':1,'g':1,'h':1,'i':1,'j':1,'k':1,'l':1,'m':1,'n':1,'o':1,'p':1,'q':1,'r':1,'s':1,'t':1,'u':1,'v':1,'w':1,'x':1,'y':1,'z':1,'A':1,'B':1,'C':1,'D':1,'E':1,'F':1,'G':1,'H':1,'I':1,'J':1,'K':1,'L':1,'M':1,'N':1,'O':1,'P':1,'Q':1,'R':1,'S':1,'T':1,'U':1,'V':1,'W':1,'X':1,'Y':1,'Z':1
        }
        ,math:{
          'PLUS':1,'ADD':1,'DASH':1,'SUBTRACT':1,'FSL':1,'DIVIDE':1,'AST':1,'MULTIPLY':1,'EQUALS':1,'PCT':1,'CARET':1,'TILDE':1
        }
        ,logic:{
          'VBAR':1,'AMP':1
        }
        ,pairs:{
          'LBKT':1,'RBKT':1,'LBRC':1,'RBRC':1,'LP':1,'RP':1,'LT':1,'GT':1
        }
        ,punct:{
          'COMMA':1,'PERIOD':1,'DECIMAL':1,'SEMI':1,'COLON':1,'QM':1,'BANG':1
        }
        ,quot:{
          'SQ':1,'QUOTE':1,'BQ':1
        }
        ,other:{
          'AT':1,'HASH':1,'BUCK':1,'BAR':1,'BSL':1,'SPACE':1
        }

      }
      // m r w h t l
      // m - margin right in key units
      // r - row
      // w - width in key units
      // h - height in key units
      // t - text || keyindex
      // l - leading margin
      //
      //MARKER KEYS                          CLASS
      // U      ~!@#$%^&*()_+{}:"|<>?         shifter
      // u      ~!@#$%^&*()_+{}:"|<>?a-z      unshift
      // o      a-z                           lowcase
      // A      A-Z                           upcase
      // n      r0-9                          num
      // N      numpad0-9                     numpadnum
      ,rows:[
        {s:'ESC F1 F2 F3 F4 F5 F6 F7 F8 F9 F10 F11 F12 PRTSCR SCRLK PAUSE X1 X2 X3 X4',r:0}
        ,{s:'BQ 1 2 3 4 5 6 7 8 9 0 DASH EQUALS BACKSPACE INS HOME PGUP NUMLK DIVIDE MULTIPLY SUBTRACT',r:1}
        ,{s:"TILDE BANG AT HASH BUCK PCT CARET AMP AST LP RP BAR PLUS",r:1,u:1}
        ,{s:'TAB Q W E R T Y U I O P LBKT RBKT BSL DEL END PGDN NUM7 NUM8 NUM9 ADD',r:2}
        ,{s:"q w e r t y u i o p LBRC RBRC VBAR",r:2,u:1}
        ,{s:'CAPS A S D F G H J K L SEMI SQ ENTER NUM4 NUM5 NUM6',r:3}
        ,{s:"a s d f g h j k l COLON QUOTE",r:3,u:1}
        ,{s:'LSHIFT Z X C V B N M COMMA PERIOD FSL RSHIFT UP NUM1 NUM2 NUM3 NUMENTER',r:4}
        ,{s:"z x c v b n m LT GT QM",r:4,u:1}
        ,{s:'LCTRL LWIN LALT SPACE RALT RWIN FN RCTRL LEFT DOWN RIGHT NUM0 DECIMAL',r:5}
      ]
    }
  }
};

///
(function(){
  window.keylay.build=function(){
    var that=this;
    for(var kb in this.kbds) {
      if(!this.kbds.hasOwnProperty(kb)) {
        continue;
      }
      if(typeof this.kbds[kb]!=='object') {
        continue;
      }
      var kbd=this.kbds[kb];//
      var vals=kbd.vals;
      var rows=kbd.rows;
      rows.forEach(function(v){
        var row=v;
        var s=row.s;
        var r=row.r;
        var vo={};
        for(var i in row) {
          if(!row.hasOwnProperty(i)||i==='s'||i==='r') {
            continue;
          }
          vo[i]=row[i];
        }
        var a=s.split(' ');
        a.forEach(function(key,i){
          var no={
            r:r
            ,x:i
          };
          $.extend(true,no,vo);
          for(var j in vals) {//m r w h l t
            var jo=vals[j];
            if(jo[key]!==undefined) {
              no[j]=jo[key];
            }
          }
          // create classes/
          var classes=[];
          if(no.r!==undefined) {
            classes.push('row'+no.r);
          }
          for(var e in no) {
            if(!no.hasOwnProperty(e)) {
              continue;
            }
            if(that.cfg.passvals.indexOf(e)!==-1) {
              continue;
            }
            if(that.classmap[e]===undefined) {
              classes.push(e);
              delete no[e];
              //continue;
            }else{
              classes.push(that.classmap[e]);
              delete no[e];
            }
          }
          no.classes=classes;
          // add to
          window.keylay.keys[key]=no;
          window.keylay.order.push(key);
        });
      });
    }
  };
  keylay.build();
}());





