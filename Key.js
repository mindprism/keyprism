function Key(opts){
  //this.name=opts.name;
  $.extend(true,this,opts);
}
inhf(Key.prototype,{
  draw:function($el){
    $el.html(this.name);
  }
  ,_createLab:function(id,o){
    this.labels[id]=new KeyLabel(o);
    this.labels[id].create();
  }
  ,create:function($el){
    if(this._el===undefined){
      this._container=$el;
      var id='key_'+this.name;
      var cls='key';
      //console.log(this.lay.classes);
      //cls+=' row'+this.lay.r;
      if(this.lay.classes!==undefined&&this.lay.classes.length!=0) {
        cls+=' '+this.lay.classes.join(' ');
      }
      var s='';
      s+='<div';
      s+=' id="'+id+'"';
      s+=' class="'+cls+'"';
      s+='>'+'</div>';
      $el.append(s);
      this._el=$el.find('#key_'+this.name);
      this.labels={};
      var o={parent:this};
      o.name=this.name;
      o.text=this.text;//.toUpperCase();
      o.type='key';
      o.desc=this.name;
      this._createLab('key',o);
      delete o.desc;
      // p s as a ca c cs cas
      var m='p s as a ca c cs cas'.split(' ');
      for(var x=0;x<m.length;x++){
        if(this.items[m[x]]!==undefined){
          o.name=m[x];
          o.text=this.items[m[x]].t;
          o.type=m[x].toUpperCase();
          o.desc=this.items[m[x]].d;
          this._createLab(m[x],o);
        }else{
          o.name=m[x];
          o.text='';
          o.type=m[x].toUpperCase();
          o.desc='unassigned';
          this._createLab(m[x],o);
        }
      }
    }
  }
});

