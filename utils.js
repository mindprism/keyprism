function inhf(dest,src){
  for(var p in src){
    //console.log(p);
    if(!src.hasOwnProperty(p)){
      continue;
    }
    //console.log(p);
    if(typeof src[p]==='function'){
      dest[p]=src[p];
    }
  }
}

function removeCss(id){
  if(id!==undefined){
    $('#'+id).remove();
  }
}
function addCss(css,id){
  removeCss(id);
  var style = document.createElement('style');
  if(id!==undefined){
    style.id=id;
  }
  style.type = 'text/css';
  style.innerHTML = css;
  document.getElementsByTagName('head')[0].appendChild(style);
}

