


function cssForNone(id){
  var NL="\n";
  var W=70;
  var H=150;
  var idf='#key_'+id;//.toUpperCase();
  var el=$(idf);
  var hh=el.height();
  var offs=el.offset();
  var nx=offs.left;
  var ny=offs.top;
  var o=keylay.keys[id];
  var s='';
  var body_class='.layout-none';
  s+=body_class+' '+idf;
  s+='{'+NL;
  s+='  position:absolute;';
  s+='  left:'+nx+'px;';
  s+='  top:'+ny+'px;';
  s+='transition:all 1200ms;';
  s+='}'+NL;
  return {css:s};
}


function cssForKey(id,cx,cy){
  var NL="\n";
  var W=70;
  var H=150;
  var idf='#key_'+id;//.toUpperCase();
  var el=$(idf);
  var hh=el.height();
  H=hh;

  var nx=cx;
  var ny=cy;
  var o=keylay.keys[id];
  if(o.x===0){
    nx=0;
    cx=0;
    ny=cy+H;
  }
  if (o.l!==undefined){
    nx+=parseInt(''+(W*o.l),10);
    cx+=parseInt(''+(W*o.l),10);
  }
  //console.log('o.x:'+o.x);
  var nw=W;
  if(o.w!==undefined){
    nw=parseInt(''+(nw*o.w),10);
  }
  var s='';
  var body_class='.layout-keyboard';
  s+=body_class+' '+idf;
  s+='{'+NL;
  s+='  position:absolute;';
  s+='  left:'+nx+'px;';
  s+='  top:'+ny+'px;';
  s+='  width:'+nw+'px;';
  if(o.h!==undefined){
    s+='  height:'+(hh*o.h+4)+'px;';
  }
  s+='transition:all 1200ms;';
  s+='}'+NL;
  cx+=nw;
  if(o.m!==undefined){
    cx+=parseInt(''+(W*o.m),10);
  }else{
    //cx=nx+W;
  }
  cy=ny;
  return {css:s,x:cx,y:cy};
}

function hideKeylabelsFontCss(tf){
  var s='.key .keylabel:not(.key){font-size:0px;}';
  if(tf){
    addCss(s,'hideKeylabelsFont');
  }else{
    removeCss('hideKeylabelsFont');
  }
}

function buildCssForNone(){
  var tgt=$('#tgt');
  var css='';
  css='.layout-none .key .keylabel:not(.key){font-size:0px;}';
  for(var i in keylay.keys){
    var oo=cssForNone(i);
    css+=oo.css;
  }
  addCss(css,'nolayout');
  hideKeylabelsFontCss(false);
}


function buildKeys0(){
  var tgt=$('#tgt');
  var x;
  var keys=Object.keys(keylay.keys);
  hideKeylabelsFontCss(true);
  keys.sort();
  for(var x=0;x<keys.length;x++){
    var i=keys[x];
    var o={};
    var name=i;//.toUpperCase();
    var kso=keyset[i];
    var txt=i;
    if(keylay.keys[i].t!==undefined){
      txt=keylay.keys[i].t;
    }
    kso=kso||{items:{}};
    o.name=name;
    o.text=txt;
    //o.name=i.toUpperCase();
    $.extend(true,o,{lay:keylay.keys[i]});
    //console.log('o1',o);
    $.extend(true,o,kso);
    //console.log('o2',o);
    var k=new Key(o);
    k.create(tgt);
  }
}

function buildCssForKeyboard(){
  var tgt=$('#tgt');
  var x;
  var cx=0;
  var cy=0;
  var css='';
  var l=keylay.order.length;
  for(x=0;x<l;x++){
    var i=keylay.order[x];
    var oo=cssForKey(i,cx,cy);
    css+=oo.css;
    cx=oo.x;
    cy=oo.y;
    //console.log(oo.css);
  }
  addCss(css,'keylayout');
}



// m - margin right in key units
// r - row
// w - width in key units
// h - height in key units
// t - text || keyindex

function initLayoutButtons(){
  var btns=$('.layout-btn');
  btns.on('click',function(e){
    var me=$(this);
    console.log('click',this,me.attr('data-checked'));
    var classes=$('body').attr('class');
    var cls=me.attr('id');
    var istrue=me.attr('data-checked')==='true';
    if(istrue){
      return;
    }
    if(classes!==undefined&&classes!=='') {
      var a=classes.split(' ');
      a=a.filter(function(v){
        return v.indexOf('layout-')!==-1;
      });
      a.forEach(function(v){
        $('body').removeClass(v);
      });
    }
    $('body').addClass(cls);
    $('.layout-btn').attr('data-checked','false');
    me.attr('data-checked','true');
  });
}

function initCss(){
  var s=prism.qtip.css();
  addCss(s,'qtipcss');
  //console.log('ss',s);
  s=prism.keylabel.css();
  addCss(s,'keylabel');
}
function initQtips(){
  function initQtip(k){
    $('.keylabel.'+k).qtip({
      style:{
        classes:'qtip-'+k+' qtip-rounded qtip-shadow'
      }
    });
  }
  initQtip('key');
  initQtip('P');
  initQtip('S');
  initQtip('AS');
  initQtip('A');
  initQtip('CA');
  initQtip('C');
  initQtip('CS');
  initQtip('CAS');
}


$(function(){
  initCss();
  buildKeys0();///
  buildCssForNone();//
  //$('body').addClass('layout-none');
  buildCssForKeyboard();
  $('body').addClass('layout-none');
  initLayoutButtons();
  initQtips();
});


//Key.prototype.draw=function()

//greetMe("World");




