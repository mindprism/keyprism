window.keyset={
  "A":{
    items:{
      p:{
        t:'plain'
        ,d:'plain desc'
        ,g:['plain']
        ,i:['a.s']
        ,r:['a.c']
      }
      ,s:{
        t:'shift'
        ,d:'shift desc'
        ,g:['plain']
        ,i:['a.p']
        ,r:['a.c']
      }
      ,as:{
        t:'alt-shift'
        ,d:'alt-shift desc'
      }
      ,xa:{
        t:'alt'
        ,d:'alt desc'
      }
      ,ca:{
        t:'ctrl-alt'
        ,d:'ctrl-alt desc'
      }
      ,c:{
        t:'ctrl'
        ,d:'ctrl desc'
        ,r:['a.p','a.s']
      }
      ,cs:{
        t:'ctrl-shift'
        ,d:'ctrl-shift desc'
      }
      ,cas:{
        t:'ctrl-alt-shift'
        ,d:'ctrl-alt-shift desc'
      }
    }
  }
}
