window.prism={
  qtip:{
    cfg:{
      selectors:{
        '.qtip-*':{
          'background-color':{
            fn:'darken'
            ,amt:{key:70,_:40}
            ,a:0.7
          }
          ,'border-color':{
            fn:'darken'
            ,amt:31
            ,a:0.6
          }
          ,'color':{
            a:0.8
          }
          ,'text-shadow':'1px 1px #000'
        }
        ,'.qtip-* .qtip-titlebar':{
          'background-color':{
            fn:'darken'
            ,amt:{key:70,_:40}
            ,a:0.9
          }
        }
        ,'.qtip-* .qtip-icon':{
          'border-color':{
            fn:'darken'
            ,amt:31
            ,a:0.6
          }
        }
        ,'.qtip-* .qtip-titlebar .ui-state-hover':{
          'border-color':{
            fn:'darken'
            ,amt:40
            ,a:0.7
          }
        }
      }
    }
  }
  ,keylabel:{
    cfg:{
      alphas:{
        border:0.6
        ,color:0.8
        ,'box-shadow':0.3
      }
      ,alter:{
        border:{
          fn:'darken'
          ,amt:20
        }
        ,color:{
        }
        ,'box-shadow':{
          fn:'darken'
          ,amt:20
        }
      }
      ,attrs:{
        key:['border']
      }
    }
    ,items:{
      key:{
        base:{
          r:200
          ,g:200
          ,b:200
        }
        ,border:'transparent'
      }
      ,P:{
        base:{
          r:250
          ,g:250
          ,b:250
        }
      }
      ,S:{
        base:{
          r:250
          ,g:250
          ,b:0
        }
      }
      ,AS:{
        base:{
          r:0
          ,g:250
          ,b:0
        }
      }
      ,A:{
        base:{
          r:0
          ,g:250
          ,b:250
        }
      }
      ,CA:{
        base:{
          r:250
          ,g:0
          ,b:250
        }
      }
      ,C:{
        base:{
          r:250
          ,g:0
          ,b:0
        }
      }
      ,CS:{
        base:{
          r:250
          ,g:100
          ,b:0
        }
      }
      ,CAS:{
        base:{
          r:150
          ,g:150
          ,b:150
        }
      }
    }
  }
};

(function(){
  var s='P S AS A CA C CS CAS';
  var a=s.split(' ');
  a.forEach(function(v){
    prism.keylabel.cfg.attrs[v]=['border','color','box-shadow'];
  });
}());
prism.keylabel.css_=function(k){
  var that=this;
  var o=this.items[k];
  var base_color=tinycolor(o.base);
  var attrs=this.cfg.attrs[k];
  var s='';
  s+='.keylabel.'+k+'{';
  attrs.forEach(function(v){
    s+=v+':';
    if(v==='border') {
      s+='1px solid';
    }
    if(v==='box-shadow') {
      s+='inset 0 0 1.5em 0.2em';
    }
    var tcu=tinycolor(o.base);//=tinycolor(base_color);
    if(o[v]!==undefined) {
      s+=' '+o[v]+';';
    }else{
      if(that.cfg.alter[v]!==undefined) {
        if(that.cfg.alter[v].fn!==undefined) {
          tcu[that.cfg.alter[v].fn](that.cfg.alter[v].amt);
        }
      }
      if(that.cfg.alphas[v]!==undefined) {
        tcu.setAlpha(that.cfg.alphas[v]);
      }
      s+=' '+tcu.toRgbString()+';';
    }

  });
  s+='}';
  return s;
};
prism.qtip.css_=function(k){
  var that=this;
  var o=prism.keylabel.items[k];
  var selectors=Object.keys(this.cfg.selectors);
  //console.log(selectors);
  var s='';
  selectors.forEach(function(v){
    //console.log(v);
    s+=v.replace(/\*/,k)+'{';
    var sel=prism.qtip.cfg.selectors[v];
    var attrs=Object.keys(sel);
    attrs.forEach(function(vv){
      s+=vv+':';
      var attr=sel[vv];
      if(typeof attr==='string'){
        s+=attr+';';
      }else{
        //fn amt a
        var color=tinycolor(o.base);
        if(attr.fn!==undefined){
          var amt=attr.amt;
          if(typeof amt==='object'){
            if(amt[v]!==undefined){
              amt=amt[v];
            }else{
              amt=amt._;
            }
          }
          color[attr.fn](amt);
        }
        if(attr.a!==undefined){
          color.setAlpha(attr.a);
        }
        s+=color.toRgbString()+';';
      }
    });
    s+='}'+"\n";
  });
  return s;
}
prism.qtip.css=function(){
  var s='';
  var ks=Object.keys(prism.keylabel.items);
  var m=ks.map(function(v){
    //console.log(v);
    return prism.qtip.css_(v);
  });
  return m.join('\n');
  //console.log(m);
  //return 'x';
}
prism.keylabel.css=function(){
  var s='';
  var ks=Object.keys(this.items);
  var m=ks.map(function(v){
    //console.log(v);
    return prism.keylabel.css_(v);
  });
  return m.join('\n');
  //console.log(m);
  //return 'x';
}


