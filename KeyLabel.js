function KeyLabel(opts){
  $.extend(true,this,opts);
}
inhf(KeyLabel.prototype,{
  create:function(){
    //console.log('create'+this.name,this.parent._el);
    if(this._el===undefined){
      var $el=this.parent._el;//
      var txt=this.text;
      txt=txt===''?'&nbsp;':txt;
      var id='keylabel_'+this.parent.name+'_'+this.name;
      var cls='keylabel '+this.type;
      var tit=this.desc;
      var s='<div';
      s+=' id="'+id+'"';
      s+=' class="'+cls+'"';
      if (tit!==undefined){
        s+=' title="'+tit+'"';
      }
      s+='>'+txt+'</div>';
      ///console.log(s);
      $el.append(s);
      this._el=$el.find('#'+id);
    }
  }
});

